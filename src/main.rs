use rand::Rng;
use raylib::prelude::*;

const MAX_TRACK_LEN: usize = 16;
const MAX_TRACK_NUM: usize = 4;

#[derive(Clone, Copy, Debug)]
struct Cell {
    pub instrument: usize,
    pub volume: f32,
    pub pitch: f32,
}

#[derive(Clone, Copy)]
struct Track {
    pub cells: [Option<Cell>; MAX_TRACK_LEN],
}

impl Track {
    pub fn default() -> Self {
        Track {
            cells: [None; MAX_TRACK_LEN],
        }
    }
}

struct UISettings {
    pub font: Font,
    pub font_size: f32,
    pub screen_size: Vector2,
}

#[derive(PartialEq)]
enum RecordingMode {
    None,
    Instrument,
    Volume,
    Pitch,
}

struct Tracker {
    pub pattern: [Track; MAX_TRACK_NUM],
    pub length: u8,
    pub playing: bool,
    pub recording: RecordingMode,
    pub mute_mask: u8,
    pub player_pos: usize,
    pub bpm: u16,
    pub loaded_samples: Vec<Sound>,
    pub ui_settings: UISettings,
    pub audio_dev: RaylibAudio,
    pub cursor_pos: (usize, usize),
    pub user_input: String,
}

impl Tracker {
    pub fn new(rl: &mut RaylibHandle, thread: &RaylibThread) -> Self {
        let font = rl
            .load_font(
                &thread,
                "./assets/fonts/Terminus/TerminessNerdFontMono-Regular.ttf",
            )
            .unwrap();

        Tracker {
            length: 16,
            playing: false,
            recording: RecordingMode::None,
            mute_mask: 0,
            player_pos: 0,
            pattern: [Track::default(); 4],
            bpm: 140,
            loaded_samples: Vec::new(),
            ui_settings: UISettings {
                font,
                font_size: 24.0,
                screen_size: Vector2::zero(),
            },
            audio_dev: audio::RaylibAudio::init_audio_device(),
            cursor_pos: (0, 0),
            user_input: String::new(),
        }
    }

    pub fn set_instrument(&mut self, val: usize) {
        let x = self.cursor_pos.0;
        let y = self.cursor_pos.1;
        let is_cell_none = self.pattern[x].cells[y].is_none();
        if is_cell_none {
            self.pattern[x].cells[y] = Some(Cell {
                instrument: val.clamp(0, self.loaded_samples.len() - 1),
                volume: 1.0,
                pitch: 1.0,
            });
        } else {
            let curr_cell = self.pattern[x].cells[y].unwrap();
            self.pattern[x].cells[y] = Some(Cell {
                instrument: val.clamp(0, self.loaded_samples.len() - 1),
                volume: curr_cell.volume,
                pitch: curr_cell.pitch,
            });
        }
    }

    pub fn set_volume(&mut self, val: f32) {
        let x = self.cursor_pos.0;
        let y = self.cursor_pos.1;
        let is_cell_none = self.pattern[x].cells[y].is_none();
        if is_cell_none {
            self.pattern[x].cells[y] = Some(Cell {
                instrument: 0,
                volume: val.clamp(0.0, 1.5),
                pitch: 1.0,
            });
        } else {
            let curr_cell = self.pattern[x].cells[y].unwrap();
            self.pattern[x].cells[y] = Some(Cell {
                instrument: curr_cell.instrument,
                volume: val.clamp(0.0, 1.5),
                pitch: curr_cell.pitch,
            });
        }
    }

    pub fn set_pitch(&mut self, val: f32) {
        let x = self.cursor_pos.0;
        let y = self.cursor_pos.1;
        let is_cell_none = self.pattern[x].cells[y].is_none();
        if is_cell_none {
            self.pattern[x].cells[y] = Some(Cell {
                instrument: 0,
                volume: 1.0,
                pitch: val.clamp(-3.0, 3.0),
            });
        } else {
            let curr_cell = self.pattern[x].cells[y].unwrap();
            self.pattern[x].cells[y] = Some(Cell {
                instrument: curr_cell.instrument,
                volume: curr_cell.volume,
                pitch: val.clamp(-3.0, 3.0),
            });
        }
    }

    pub fn next_step(&mut self) {
        let curr_pos = self.player_pos;
        let mut curr_cells: Vec<Cell> = Vec::new();
        let mut idx = 0;
        for t in self.pattern {
            if 1 & (self.mute_mask >> (idx)) != 1 {
                if let Some(c) = t.cells[curr_pos] {
                    curr_cells.push(c);
                }
            }
            idx += 1;
        }
        for c in curr_cells {
            let cell_sound = self.loaded_samples.get(c.instrument);
            if cell_sound.is_none() {
                println!("  > WARNING: No sample with index {i}", i = c.instrument);
                continue;
            }

            let sample = cell_sound.unwrap();

            self.audio_dev.set_sound_pitch(sample, c.pitch);
            self.audio_dev.set_sound_volume(sample, c.volume);
            self.audio_dev.play_sound(sample);
        }
        self.player_pos = (self.player_pos + 1) % self.length as usize;
    }

    pub fn draw(&self, mut d: RaylibDrawHandle) {
        let mut row: usize = 0;
        let mut col: usize;

        let cell_w: usize = 200;
        let cell_h: usize = 25;

        let x_margin = 5.0;
        // let x_margin: f32 =
        //     (self.ui_settings.screen_size.x - (self.pattern.len() * cell_w) as f32) / 2.0;
        let y_margin: f32 = 100.0;

        let text_dimensions = measure_text_ex(
            &(self.ui_settings.font),
            " ---   ---   --- ",
            self.ui_settings.font_size,
            1.0,
        );
        let text_length = text_dimensions.x;
        let text_height = text_dimensions.y;

        let bg = Color::from_hex("1a1b26").unwrap();
        let bg_dark = Color::from_hex("15161e").unwrap();
        let hl = Color::from_hex("414868").unwrap();
        let fg = Color::from_hex("c0caf5").unwrap();
        let r = Color::from_hex("f7768e").unwrap();
        let g = Color::from_hex("0ece6a").unwrap();
        let b = Color::from_hex("7aa2f7").unwrap();
        let m = Color::from_hex("bb9af7").unwrap();

        d.clear_background(bg);

        let status_text;
        let mut status_text_color;
        if self.playing {
            status_text = "PLAYING";
            status_text_color = g;
        } else {
            status_text = "PAUSED";
            status_text_color = b;
        }
        let mut recording_text = "";
        let fmt: String;
        if self.recording != RecordingMode::None {
            // recording_text = "(recording)";
            status_text_color = r;
            recording_text = match self.recording {
                RecordingMode::Instrument => {
                    fmt = format!(
                        "( [ {} ; {} ]: Instrument = [ {:-<3} ] )",
                        self.cursor_pos.0 + 1,
                        self.cursor_pos.1 + 1,
                        self.user_input
                    );
                    fmt.as_str()
                }
                RecordingMode::Volume => {
                    fmt = format!(
                        "( [ {} ; {} ]: Volume = [ {:-<3} ] )",
                        self.cursor_pos.0 + 1,
                        self.cursor_pos.1 + 1,
                        self.user_input
                    );
                    fmt.as_str()
                }
                RecordingMode::Pitch => {
                    fmt = format!(
                        "( [ {} ; {} ]: Pitch = [ {:-<3} ] )",
                        self.cursor_pos.0 + 1,
                        self.cursor_pos.1 + 1,
                        self.user_input
                    );
                    fmt.as_str()
                }
                _ => "",
            }
        }

        d.draw_text_ex(
            &self.ui_settings.font,
            format!("bpm: {:3} | {status_text:7} {recording_text}", self.bpm).as_str(),
            Vector2::new(x_margin * 3 as f32, y_margin / 4 as f32),
            self.ui_settings.font_size - 4.0,
            1.0,
            status_text_color,
        );

        for t in self.pattern {
            col = 0;
            for c in t.cells {
                let position = Vector2::new(
                    x_margin + (row * cell_w) as f32 + (text_length / 2.0),
                    y_margin + (col * cell_h) as f32,
                );
                d.draw_text_ex(
                    &self.ui_settings.font,
                    format!("{:3}", col + 1).as_str(),
                    Vector2::new(x_margin * 12 as f32, y_margin + (col * cell_h) as f32 + 3.0),
                    self.ui_settings.font_size - 4.0,
                    1.0,
                    fg,
                );
                d.draw_text_ex(
                    &self.ui_settings.font,
                    " [I] | [V] | [P]",
                    Vector2::new(position.x, y_margin / 1.40),
                    self.ui_settings.font_size,
                    1.0,
                    hl,
                );

                if col == self.player_pos {
                    d.draw_rectangle(
                        position.x as i32,
                        position.y as i32,
                        (f32::ceil(text_length)) as i32 + 2,
                        (f32::ceil(text_height)) as i32,
                        hl,
                    );
                } else if col % 4 == 0 {
                    d.draw_rectangle(
                        position.x as i32,
                        position.y as i32,
                        (f32::ceil(text_length)) as i32 + 2,
                        (f32::ceil(text_height)) as i32,
                        bg_dark,
                    );
                } else {
                    d.draw_rectangle(
                        position.x as i32,
                        position.y as i32,
                        (f32::ceil(text_length)) as i32 + 2,
                        (f32::ceil(text_height)) as i32,
                        bg,
                    );
                }
                if col == self.cursor_pos.1 && row == self.cursor_pos.0 {
                    let color = match self.recording {
                        RecordingMode::None => m,
                        _ => r,
                    };
                    d.draw_rectangle_lines(
                        position.x as i32,
                        position.y as i32,
                        (f32::ceil(text_length)) as i32 + 2,
                        (f32::ceil(text_height)) as i32,
                        color,
                    );
                } else {
                    d.draw_rectangle_lines(
                        position.x as i32,
                        position.y as i32,
                        (f32::ceil(text_length)) as i32 + 2,
                        (f32::ceil(text_height)) as i32,
                        bg_dark,
                    );
                }
                if let Some(c) = c {
                    let text =
                        format!(" {:03}   {:3.1}   {:3.1} ", c.instrument, c.volume, c.pitch);
                    d.draw_text_ex(
                        &self.ui_settings.font,
                        text.as_str(),
                        position,
                        self.ui_settings.font_size,
                        1.0,
                        fg,
                    );
                } else {
                    let text = " ---   ---   --- ";
                    d.draw_text_ex(
                        &self.ui_settings.font,
                        text,
                        position,
                        self.ui_settings.font_size,
                        1.0,
                        fg,
                    );
                }
                col += 1;
            }
            row += 1;
        }
    }
}

fn main() {
    let w = 1000;
    let h = 800;
    let title = "TRKR";
    let (mut rl, thread) = raylib::init().size(w, h).title(title).build();

    let mut tracker = Tracker::new(&mut rl, &thread);
    tracker.ui_settings.screen_size =
        Vector2::new(rl.get_screen_width() as f32, rl.get_screen_height() as f32);

    tracker.pattern[0].cells[0] = Some(Cell {
        instrument: 0,
        volume: 1.0,
        pitch: 1.0,
    });
    tracker.pattern[0].cells[4] = Some(Cell {
        instrument: 0,
        volume: 1.0,
        pitch: 1.0,
    });
    tracker.pattern[0].cells[8] = Some(Cell {
        instrument: 0,
        volume: 1.0,
        pitch: 1.0,
    });
    tracker.pattern[0].cells[11] = Some(Cell {
        instrument: 0,
        volume: 1.0,
        pitch: 1.0,
    });
    tracker.pattern[0].cells[14] = Some(Cell {
        instrument: 0,
        volume: 1.0,
        pitch: 1.0,
    });

    tracker.pattern[1].cells[4] = Some(Cell {
        instrument: 1,
        volume: 0.75,
        pitch: 1.0,
    });
    tracker.pattern[1].cells[12] = Some(Cell {
        instrument: 1,
        volume: 0.75,
        pitch: 1.0,
    });

    let mut rng = rand::thread_rng();
    for idx in 0..tracker.pattern[2].cells.len() {
        tracker.pattern[2].cells[idx] = Some(Cell {
            instrument: 2,
            volume: 0.25 + rng.gen_range(-0.125..0.125),
            pitch: 1.0 + rng.gen_range(-0.125..0.125),
        });
    }

    tracker.pattern[3].cells[2] = Some(Cell {
        instrument: 3,
        volume: 0.6,
        pitch: 1.0,
    });
    tracker.pattern[3].cells[5] = Some(Cell {
        instrument: 3,
        volume: 0.6,
        pitch: 1.0,
    });
    tracker.pattern[3].cells[7] = Some(Cell {
        instrument: 3,
        volume: 0.6,
        pitch: 1.0,
    });
    tracker.pattern[3].cells[8] = Some(Cell {
        instrument: 3,
        volume: 0.6,
        pitch: 1.0,
    });
    tracker.pattern[3].cells[11] = Some(Cell {
        instrument: 3,
        volume: 0.6,
        pitch: 1.0,
    });
    tracker.pattern[3].cells[14] = Some(Cell {
        instrument: 3,
        volume: 0.6,
        pitch: 1.0,
    });

    tracker
        .loaded_samples
        .push(audio::Sound::load_sound("./assets/test_samples/kick.mp3").unwrap());
    tracker
        .loaded_samples
        .push(audio::Sound::load_sound("./assets/test_samples/clap.mp3").unwrap());
    tracker
        .loaded_samples
        .push(audio::Sound::load_sound("./assets/test_samples/hihat.mp3").unwrap());
    tracker
        .loaded_samples
        .push(audio::Sound::load_sound("./assets/test_samples/piano.mp3").unwrap());

    let mut timer: f32 = 0.0;

    while !rl.window_should_close() {
        let d = rl.begin_drawing(&thread);
        tracker.draw(d);

        let pressed_key_option = rl.get_key_pressed();
        if pressed_key_option.is_some() {
            let pressed_key = pressed_key_option.unwrap();
            if tracker.recording == RecordingMode::None {
                match pressed_key {
                    KeyboardKey::KEY_SPACE => {
                        tracker.playing = !tracker.playing;
                    }
                    KeyboardKey::KEY_ENTER => {
                        tracker.playing = false;
                        tracker.player_pos = 0;
                    }
                    KeyboardKey::KEY_EQUAL => {
                        tracker.bpm += 1;
                    }
                    KeyboardKey::KEY_MINUS => {
                        tracker.bpm -= 1;
                    }
                    KeyboardKey::KEY_LEFT => {
                        if tracker.cursor_pos.0 > 0 {
                            tracker.cursor_pos.0 -= 1;
                        }
                    }
                    KeyboardKey::KEY_RIGHT => {
                        if tracker.cursor_pos.0 < tracker.pattern.len() - 1 {
                            tracker.cursor_pos.0 += 1;
                        }
                    }
                    KeyboardKey::KEY_UP => {
                        if tracker.cursor_pos.1 > 0 {
                            tracker.cursor_pos.1 -= 1;
                        }
                    }
                    KeyboardKey::KEY_DOWN => {
                        if tracker.cursor_pos.1 < tracker.pattern[0].cells.len() - 1 {
                            tracker.cursor_pos.1 += 1;
                        }
                    }
                    KeyboardKey::KEY_I => {
                        tracker.recording = RecordingMode::Instrument;
                    }
                    KeyboardKey::KEY_V => {
                        tracker.recording = RecordingMode::Volume;
                    }
                    KeyboardKey::KEY_P => {
                        tracker.recording = RecordingMode::Pitch;
                    }
                    KeyboardKey::KEY_BACKSPACE => {
                        tracker.pattern[tracker.cursor_pos.0].cells[tracker.cursor_pos.1] = None;
                    }
                    KeyboardKey::KEY_ONE => {
                        tracker.mute_mask ^= 1 << (0);
                    }
                    KeyboardKey::KEY_TWO => {
                        tracker.mute_mask ^= 1 << (1);
                    }
                    KeyboardKey::KEY_THREE => {
                        tracker.mute_mask ^= 1 << (2);
                    }
                    KeyboardKey::KEY_FOUR => {
                        tracker.mute_mask ^= 1 << (3);
                    }
                    _ => {}
                }
            } else {
                if let Some(rl_key) = pressed_key_option {
                    let key = rl_key as u8;
                    if tracker.user_input.len() < 3 && ((key >= 48 && key <= 57) || key == 46) {
                        let key_char = key as u8 as char;
                        tracker.user_input.push(key_char);
                    } else if rl_key == KeyboardKey::KEY_BACKSPACE {
                        tracker.user_input.pop();
                    } else if rl_key == KeyboardKey::KEY_ENTER {
                        match tracker.recording {
                            RecordingMode::Instrument => {
                                let val = tracker.user_input.parse::<usize>();
                                if let Ok(val) = val {
                                    tracker.set_instrument(val);
                                }
                            }
                            RecordingMode::Volume => {
                                let val = tracker.user_input.parse::<f32>();
                                if let Ok(val) = val {
                                    tracker.set_volume(val);
                                }
                            }
                            RecordingMode::Pitch => {
                                let val = tracker.user_input.parse::<f32>();
                                if let Ok(val) = val {
                                    tracker.set_pitch(val);
                                }
                            }
                            _ => {}
                        }
                        tracker.recording = RecordingMode::None;
                        tracker.user_input = String::new();
                    }
                }
            }
        }

        if tracker.playing {
            let step_duration = 60.0 / (tracker.bpm as f32 * 4.0);
            timer += rl.get_frame_time();
            if timer >= step_duration {
                tracker.next_step();
                timer = 0.0;
            }
        }
    }
}
